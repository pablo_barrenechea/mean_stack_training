'use strict';

// Technologies controller
angular.module('technologies').controller('TechnologiesController', ['$scope', '$stateParams', 
                                          '$location', 'Authentication', 'Technologies', 'Tags',
                                          'Questions',
	function($scope, $stateParams, $location, Authentication, Technologies, Tags, Questions) {
		$scope.authentication = Authentication;
        $scope.currentTech = undefined;
        $scope.currentTechnologyQuestions = [];
        $scope.questionModalVisible = false;
		// Create new Technology
		$scope.create = function() {
			// Create new Technology object
			var technology = new Technologies ({
				name: this.name,
				description: this.description
			});

			// Redirect after save
			technology.$save(function(response) {
				$location.path('technologies' /*+'/'+ response._id*/);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Technology
		$scope.remove = function(technology) {
			if ( technology ) { 
				technology.$remove();

				for (var i in $scope.technologies) {
					if ($scope.technologies [i] === technology) {
						$scope.technologies.splice(i, 1);
					}
				}
			} else {
					$scope.technology.$remove(function() {
					$location.path('technologies');
				});
			}
		};

		// Update existing Technology
		$scope.update = function() {
			var technology = $scope.technology;
			var tagsArray = [];

			angular.forEach(technology.tags, function(value, key) {
			  tagsArray.push(value._id);
			});

			technology.tags = tagsArray;

			technology.$update(function() {
				$location.path('technologies');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Technologies
		$scope.find = function() {
			$scope.technologies = Technologies.query();
		};

		$scope.getAllTags = function(){ //Get all tags and save them on a hash table
			Tags.query(function(tags){
				$scope.tagsHashtable = {};
				angular.forEach(tags, function(obj){
					$scope.tagsHashtable[obj._id] = obj.name; // Table is index by Id.
				});
			});	
		};

		// Find existing Technology
		$scope.findOne = function() {
			$scope.technology = Technologies.get({ 
				technologyId: $stateParams.technologyId
			},
			function(t){ //With each technology also tags should be selected
				$scope.tags = Tags.query(function(tags){
					$scope.tagsArray = [];
					angular.forEach(t.tags, function(value, key) { //Check for each technologie tag
						angular.forEach(tags, function(tag){       //If it exists on tags list
							if (value === tag._id) {
								$scope.tagsArray.push(tag);	       //Add obj to scope
							}
						});
					});
					$scope.technology.tags = $scope.tagsArray;
				});
			}
			);
		};

		$scope.getTagName = function(t){ //Gets the name on a given tag id
			return $scope.tagsHashtable[t];
		};

		$scope.openModal = function(techId){
			//$dialog.dialog({}).open('../../../questions/views/view-question-modal.client.view.html'); 
           Technologies.get({ 
                technologyId: techId
            },function(t){
                 $scope.currentTech = t;
                 Questions.query({technology: techId}, function(q){
                     $scope.currentTechnologyQuestions = q;
                 });
            });
        };
}]);