'use strict';

angular.module('core').directive('ngConfirmClick', [
    function(){
        return {
			restrict: 'A',        	
            link: function (scope, element, attr) {
            	var p = attr.ngConfirmClick.split(',');
            	var clickAction = p[0];
                var msg = p[1] || 'Are you sure?';
                element.bind('click',function (event) {
                console.log(clickAction);
                    if ( clickAction ) {
                    	if ( window.confirm(msg) ) {
                        	scope.$eval(clickAction);
                    	}
                    }
                });
            }
        };
}]);