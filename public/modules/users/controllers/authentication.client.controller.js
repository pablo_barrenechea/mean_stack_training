'use strict';

var AuthenticationCons = {
	ERROR_PARAMS: 3
};

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		$scope.errorMessages = {
		    '0': 'An error happened trying to log in the user',
		    '1' : 'The domain of the account is not valid'
		};
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');
		var path = $location.path();
		//If there was an error on the signin, we need to recover the error message and show it
		if( path.match(/signinError/g) ){
			var urlParams = path.split('/');
			if( urlParams.length >= AuthenticationCons.ERROR_PARAMS )
				$scope.authentication.errorMessage = $scope.errorMessages[urlParams[2]];
		}


		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;
				console.log(response);
				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);