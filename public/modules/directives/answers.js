'use strict';
angular.module('core').
directive('answers', function() {
	return {
		restrict: 'E',
		templateUrl: '/modules/questions/views/view-answers.client.view.html',
		replace: true,
		controller: function($scope) {
			$scope.answerBoxVisible = true;
			/**
		 	* Adds answers to the collections
		 	*/

			$scope.addAnswer = function() {
				$scope.answers.push({content:'',isValid:false});
			};

			$scope.setAnswer = function(answer, index) {
				$scope.answers[index] = answer;
			};

			$scope.removeAnswer = function(index){
				$scope.answers.splice(index,1);
			};
		}
	};
});