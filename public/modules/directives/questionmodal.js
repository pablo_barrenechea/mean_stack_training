'use strict';
angular.module('core').
directive('questionmodal', function() {
    return {
        restrict: 'E',
        templateUrl: '/modules/questions/views/view-question-modal.client.view.html',
        replace: true,
        transclude: true,
        controller: function ($scope) {
             $scope.handler = 'pop';
             
             $scope.$watch('questionModalVisible', function() {
                if ($scope.questionModalVisible) {
                  //  $("#questionModalDialog").modal('show');
                }
             });
             
             $scope.closeDialog = function(){
                 $scope.questionModalVisible = false;
                //  $("#questionModalDialog").modal('hide');
             };
        }
    };
});
