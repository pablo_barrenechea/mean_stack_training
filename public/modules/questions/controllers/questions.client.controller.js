'use strict';

var levelOptions = ['Basic','Intermediate', 'High'];
var questionTypeOptions = ['Single choice', 'Multiple choice', 'Keyword based'];
// Questions controller
angular.module('questions').controller('QuestionsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Questions','Technologies',
	function($scope, $stateParams, $location, Authentication, Questions, Technologies) {
		
		$scope.authentication = Authentication;
		$scope.levelOptions = levelOptions;
		$scope.questionTypes = questionTypeOptions;
		$scope.technologies = Technologies.query();
        
		$scope.answers = [];    
        
		// Create new Question
		$scope.create = function() {
			// Create new Question object
			var question = new Questions ({
				content: this.content,
				level: this.level,
				questionType: this.questionType,
				score: this.score,
				technology: this.tech._id,
				answers: this.answers
			});
			
			// Redirect after save
			question.$save(function(response) {
				$location.path('questions/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Question
		$scope.remove = function(question) {
			if ( question ) { 
				question.$remove();

				for (var i in $scope.questions) {
					if ($scope.questions [i] === question) {
						$scope.questions.splice(i, 1);
					}
				}
			} else {
				$scope.question.$remove(function() {
					$location.path('questions');
				});
			}
		};

		$scope.mapUpdateFields = function(question){
			if(!!question.technology._id)
				question.technology = question.technology._id;//Mapping Object id to a technology
			return question;
		};

		// Update existing Question
		$scope.update = function() {
			var question = $scope.question;
			question = $scope.mapUpdateFields(question);

			question.$update(function() {
				$location.path('questions/' + question._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Questions
		$scope.find = function() {
			$scope.questions = Questions.query();
		};

		// Find existing Question
		$scope.findOne = function() {
			$scope.question = Questions.get({ 
				questionId: $stateParams.questionId
			}, function(err, obj){
				for( var i = 0; i < $scope.technologies.length; i++ ){
					if( $scope.question.technology === $scope.technologies[i]._id){
						$scope.currentTechnology = $scope.technologies[i];
						$scope.question.technology = $scope.currentTechnology;
						break;
					}
				}
			});
		};

		$scope.getTechnologyName = function(t){ //Gets the name on a given technology id
			console.log('Technology Id sent from the UI: ' + t);
			for( var i = 0; i < $scope.technologies.length; i++ ){
					if( t === $scope.technologies[i]._id){
						return $scope.technologies[i].name;
					}
				}

			return 'No technology is related to this question.';
		};

	}
]);