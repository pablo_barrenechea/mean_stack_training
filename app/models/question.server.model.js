'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var _ = require('lodash');
/**
 * Question Schema
 */
 
/**
 ** Custom validation for answers.
 ** An answer must have at least one question.
 **/

var answerValidator =  [
	{validator: function(value){
 					return (value.length > 0);
 				}, 
 	msg: 'A question must have at least one answer'}
];

var QuestionSchema = new Schema({
	content: {
		type: String,
		default: '',
		required: 'Please fill Question content',
		trim: true
	},
	score: {
		type: Number,
		default: 1,
		required: 'Please fill Question score'
	},
	level: {
		type: String,
		default: '',
		required: 'Please fill Question difficulty level',
		trim: true,
		enum: ['Basic', 'Intermediate', 'High']
	},

	questionType: {
			type: String,
			default: '',
			required: 'Please fill Question type',
			trim: true,
			enum: ['Single choice', 'Multiple choice', 'Keyword based']
	},

	technology: {
		type: Schema.ObjectId,
		ref: 'technologies',
		required: 'The question must be associated to a technology'
	},
	answers: { type: [{
		content: {
			type: String,
			default: '',
			required: 'Please, fill Answer content',
			trim: true
		},
		isValid: {
			type: Boolean,
			default: false
		},
		
	}],
	validate: answerValidator},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

var Question = mongoose.model('Question', QuestionSchema);

