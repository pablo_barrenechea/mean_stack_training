'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Technology Schema
 */
var TechnologySchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Technology name',
		trim: true,
		lowercase: true,
		unique: true,
	},
	description: {
		type: String,
		default: '',
		required: 'Please fill Description'
	},
	tags: {
		type: [{
			type: mongoose.Schema.ObjectId,
			ref: 'Tag'
		}]
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Technology', TechnologySchema);