'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var technologies = require('../../app/controllers/technologies.server.controller');

	// Technologies Routes
	app.route('/technologies')
		.get(technologies.list)
		.post(users.requiresLogin, technologies.create);

	app.route('/technologies/:technologyId')
		.get(technologies.read)
		.put(users.requiresLogin, technologies.update)
		.delete(users.requiresLogin, technologies.delete);

	// Finish by binding the Technology middleware
	app.param('technologyId', technologies.technologyByID);
};
