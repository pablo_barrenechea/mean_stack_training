'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Technology = mongoose.model('Technology');

/**
 * Globals
 */
var user, technology;

/**
 * Unit tests
 */
describe('Technology Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Pablo',
			lastName: 'Barrenechea',
			displayName: 'Pablo Barrenechea',
			email: 'pbarrenechea@devspark.com',
			username: 'palillo',
			password: 'palillo'
		});

		user.save(function() { 
			technology = new Technology({
				name: 'PHP',
				description: 'PHP is an interpreted scripting language that is embedded within an HTML web page in order to add dynamic processing to that page. The learning curve of the language in overall is a medium level.',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return technology.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			technology.name = '';

			return technology.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		Technology.remove().exec();
		User.remove().exec();

		done();
	});
});