'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Technology = mongoose.model('Technology'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, technology;

/**
 * Technology routes tests
 */
describe('Technology CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Technology
		user.save(function() {
			technology = {
				name: 'Technology Name'
			};

			done();
		});
	});

	it('should be able to save Technology instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Technology
				agent.post('/technologies')
					.send(technology)
					.expect(200)
					.end(function(technologySaveErr, technologySaveRes) {
						// Handle Technology save error
						if (technologySaveErr) done(technologySaveErr);

						// Get a list of Technologies
						agent.get('/technologies')
							.end(function(technologiesGetErr, technologiesGetRes) {
								// Handle Technology save error
								if (technologiesGetErr) done(technologiesGetErr);

								// Get Technologies list
								var technologies = technologiesGetRes.body;

								// Set assertions
								(technologies[0].user._id).should.equal(userId);
								(technologies[0].name).should.match('Technology Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Technology instance if not logged in', function(done) {
		agent.post('/technologies')
			.send(technology)
			.expect(401)
			.end(function(technologySaveErr, technologySaveRes) {
				// Call the assertion callback
				done(technologySaveErr);
			});
	});

	it('should not be able to save Technology instance if no name is provided', function(done) {
		// Invalidate name field
		technology.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Technology
				agent.post('/technologies')
					.send(technology)
					.expect(400)
					.end(function(technologySaveErr, technologySaveRes) {
						// Set message assertion
						(technologySaveRes.body.message).should.match('Please fill Technology name');
						
						// Handle Technology save error
						done(technologySaveErr);
					});
			});
	});

	it('should be able to update Technology instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Technology
				agent.post('/technologies')
					.send(technology)
					.expect(200)
					.end(function(technologySaveErr, technologySaveRes) {
						// Handle Technology save error
						if (technologySaveErr) done(technologySaveErr);

						// Update Technology name
						technology.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Technology
						agent.put('/technologies/' + technologySaveRes.body._id)
							.send(technology)
							.expect(200)
							.end(function(technologyUpdateErr, technologyUpdateRes) {
								// Handle Technology update error
								if (technologyUpdateErr) done(technologyUpdateErr);

								// Set assertions
								(technologyUpdateRes.body._id).should.equal(technologySaveRes.body._id);
								(technologyUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Technologies if not signed in', function(done) {
		// Create new Technology model instance
		var technologyObj = new Technology(technology);

		// Save the Technology
		technologyObj.save(function() {
			// Request Technologies
			request(app).get('/technologies')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Technology if not signed in', function(done) {
		// Create new Technology model instance
		var technologyObj = new Technology(technology);

		// Save the Technology
		technologyObj.save(function() {
			request(app).get('/technologies/' + technologyObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', technology.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Technology instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Technology
				agent.post('/technologies')
					.send(technology)
					.expect(200)
					.end(function(technologySaveErr, technologySaveRes) {
						// Handle Technology save error
						if (technologySaveErr) done(technologySaveErr);

						// Delete existing Technology
						agent.delete('/technologies/' + technologySaveRes.body._id)
							.send(technology)
							.expect(200)
							.end(function(technologyDeleteErr, technologyDeleteRes) {
								// Handle Technology error error
								if (technologyDeleteErr) done(technologyDeleteErr);

								// Set assertions
								(technologyDeleteRes.body._id).should.equal(technologySaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Technology instance if not signed in', function(done) {
		// Set Technology user 
		technology.user = user;

		// Create new Technology model instance
		var technologyObj = new Technology(technology);

		// Save the Technology
		technologyObj.save(function() {
			// Try deleting Technology
			request(app).delete('/technologies/' + technologyObj._id)
			.expect(401)
			.end(function(technologyDeleteErr, technologyDeleteRes) {
				// Set message assertion
				(technologyDeleteRes.body.message).should.match('User is not logged in');

				// Handle Technology error error
				done(technologyDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Technology.remove().exec();
		done();
	});
});