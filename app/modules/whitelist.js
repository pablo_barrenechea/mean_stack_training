'use strict';


var _ = require('lodash');
var config = require('../../config/config');
var whitelist = config.domainWhitelist;

/**
 ** Check if a specific email belongs to a domain whitelist.
 ** The domain can be found on user.providerData.hd attribute (from passport module), but
 ** it is not always present. 
 ** The check will be done processing the email address
 **/
exports.validateDomain = function(email){
	var splittedEmail = email.split('@');
	if( splittedEmail.length > 1 ){
		var domain = splittedEmail[1];
		var hasDomain = _.indexOf(whitelist, domain);
		if( hasDomain >= 0 ) 
			return true;
	}
	return false;
};