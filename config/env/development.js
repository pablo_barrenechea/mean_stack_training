'use strict';

module.exports = {
	db: 'mongodb://localhost/interviewquestionsmanagersystem-dev',
	app: {
		title: 'InterviewQuestionsManagerSystem - Development Environment'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '911789924201-58m31iuvshpnfm0satmcvjptqbuc6arv.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || '8Vpi3QiRmwHoF4Cp9i3OjPQM',
		callbackURL: 'http://localhost:3000/auth/google/callback'
		             
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	},
	domainWhitelist: ['devspark.com']
};
